package org.szhao;

import java.util.Map;
import java.util.Set;

/**
 * Application entry point
 *
 * @author zsh
 * created 12/Sep/2020
 */
public class App {

    public static void main(String[] args) {

        String domain = "https://www.vulog.com";

        Map<String, Set<String>> graph = (new Crawler(domain)).crawl();

        graph.forEach((k, v) -> {
            System.out.println(k + ": ");
            v.forEach(l -> System.out.println("   " + l));
        });

    }
}
