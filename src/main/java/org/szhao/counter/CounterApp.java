package org.szhao.counter;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

/**
 * @author zsh
 * created 2020/Sep/22
 */
public class CounterApp {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch doneSignal = new CountDownLatch(1);
        String domain = "https://monzo.com";
        Worker worker = new Worker(domain, doneSignal);
        worker.start();
        doneSignal.await();

        Map<String, Set<String>> graph = worker.getGraph();
        graph.forEach((k, v) -> {
            System.out.println(k + ": ");
            v.forEach(l -> System.out.println("   " + l));
        });
    }
}
