package org.szhao.counter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.szhao.DomainLinkExtractor;
import org.szhao.JsoupDomainLinkExtractor;

import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author zsh
 * created 2020/Sep/22
 */
public class Worker {

    public static final Logger logger = LoggerFactory.getLogger(Worker.class);

    private ConcurrentHashMap<String, Boolean> visited = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, Set<String>> graph = new ConcurrentHashMap<>();

    private final DomainLinkExtractor extractor;
    private final String domain;


    private final ExecutorService executorService = Executors.newFixedThreadPool(8);

    public final AtomicInteger counter = new AtomicInteger(0);

    private final CountDownLatch done;

    public Worker(String domain, CountDownLatch done) {
        this.domain = domain;
        this.extractor = new JsoupDomainLinkExtractor(domain);
        this.done = done;
    }

    public void start() {
        executorService.submit(new CrawlTask(domain));
    }

    public ConcurrentHashMap<String, Set<String>> getGraph() {
        return graph;
    }

    public class CrawlTask implements Runnable {
        public final String url;

        public CrawlTask(String url) {
            this.url = url;
        }

        @Override
        public void run() {
            logger.info("remaining tasks: {}, visiting {}", counter.get(), url);
            Set<String> links = extractor.getChildren(url);
            graph.put(url, links);

            for (String link : links) {
                if (!visited.getOrDefault(link, false)) {
                    visited.put(link, true);
                    counter.getAndIncrement();
                    executorService.submit(new CrawlTask(link));
                }
            }
            int n = counter.getAndDecrement();
            if (n == 0) {
                executorService.shutdown();
                try {
                    executorService.awaitTermination(1, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    done.countDown();
                }
            }
        }
    }
}
