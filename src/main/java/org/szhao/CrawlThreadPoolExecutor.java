package org.szhao;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * custom thread pool executor, records last execution time
 * @author zsh
 * created 13/Sep/2020
 */
public class CrawlThreadPoolExecutor extends ThreadPoolExecutor {

    private final AtomicLong lastExecutionTimeNano = new AtomicLong(System.nanoTime());

    public CrawlThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
                                   BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        long currentSysNano = System.nanoTime();
        while (true) {
            long time = lastExecutionTimeNano.get();
            if (currentSysNano < time) break;
            if (lastExecutionTimeNano.compareAndSet(time, currentSysNano)) {
                break;
            }
        }
        super.afterExecute(r, t);
    }

    public long getLastExecutionTimeNano() {
        return lastExecutionTimeNano.get();
    }
}
