package org.szhao;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author zsh
 * created 13/Sep/2020
 */
public class JsoupDomainLinkExtractor implements DomainLinkExtractor {
    private static final Logger logger = LoggerFactory.getLogger(JsoupDomainLinkExtractor.class);

    private final String domain;

    public JsoupDomainLinkExtractor(String domain) {
        this.domain = domain;
    }

    @Override
    public String getDomain() {
        return domain;
    }

    @Override
    public Set<String> getChildren(String url) {
        Document doc;

        try {
            doc = Jsoup.connect(url).timeout(5000).get();
        } catch (IOException e) { // UnsupportedMimeTypeException, HttpStatusException 404
            logger.warn("URL: {}, {}", url, e.toString());
            return new HashSet<>();
        }

        return doc.select("a").stream().map(e -> e.attr("abs:href"))
                .filter(l -> Utils.isInDomain(l, domain))
                .map(Utils::trimURL)
                .collect(Collectors.toSet());
    }
}
