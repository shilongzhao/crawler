package org.szhao;

/**
 * @author zsh
 * created 12/Sep/2020
 */
public class Utils {

    public static String trimURL(String url) {
        if (url == null) return null;
        String r = url.split("\\?")[0].split("#")[0];
        if (r.endsWith("/")) {
            return r.substring(0, r.length() - 1);
        }
        return r;
    }

    public static boolean isInDomain(String url, String domain) {
        if (url == null || url.isEmpty()) return false;
        return url.startsWith(domain);
    }
}
