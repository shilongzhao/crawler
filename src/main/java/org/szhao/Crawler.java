package org.szhao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author zsh
 * created 13/Sep/2020
 */
public class Crawler {
    private static final Logger logger = LoggerFactory.getLogger(Crawler.class);

    private static final int NUM_THREADS = 8;
    private static final long MAX_IDLE_NANO = 10_000_000_000L; // 10s

    private final CrawlThreadPoolExecutor executorService =  new CrawlThreadPoolExecutor(NUM_THREADS, NUM_THREADS,
            0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
    private final ConcurrentHashMap<String, Set<String>> graph = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, Boolean> submitted = new ConcurrentHashMap<>();

    private final String domain;
    private final DomainLinkExtractor linkExtractor;

    public Crawler(String domain) {
        this(domain, new JsoupDomainLinkExtractor(domain));
    }

    public Crawler(String domain, DomainLinkExtractor linkExtractor) {
        this.domain = Utils.trimURL(domain);
        this.linkExtractor = linkExtractor;
    }


    Map<String, Set<String>> crawl() {
        submitted.put(domain, true);
        executorService.submit(new CrawlerTask(executorService, graph, submitted, domain, linkExtractor));

        while (true) {
            try {
                Thread.sleep(1000);
                long idleNano = System.nanoTime() - executorService.getLastExecutionTimeNano();
                if (executorService.getQueue().size() == 0 && idleNano >= MAX_IDLE_NANO) {
                    executorService.shutdown();
                    boolean terminated = executorService.awaitTermination(10, TimeUnit.SECONDS);
                    if (terminated) {
                        logger.info("terminating executor service");
                        break;
                    }
                }
            } catch (InterruptedException e) {
                logger.error(e.toString());
                break;
            }
        }

        return graph;
    }

}
