package org.szhao.tracking;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.szhao.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zsh
 * created 21/Sep/2020
 */
public class JsoupWebCrawler extends WebCrawler {
    private static final Logger logger = LoggerFactory.getLogger(JsoupWebCrawler.class);

    public JsoupWebCrawler(String startURL) {
        super(startURL);
    }

    @Override
    protected List<String> processPage(String url) {
        Document doc;
        logger.info("visiting: {}", url);
        try {
            doc = Jsoup.connect(url).timeout(5000).get();
        } catch (IOException e) {
            logger.info(e.toString());
            return new ArrayList<>();
        }

        List<String> links = doc.select("a").stream().map(e -> e.attr("abs:href"))
                .filter(u -> Utils.isInDomain(u, getDomain()))
                .map(Utils::trimURL)
                .collect(Collectors.toList());
//        logger.info("extracted links: {}", links);
        return links;
    }
}
