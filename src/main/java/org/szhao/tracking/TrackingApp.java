package org.szhao.tracking;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zsh
 * created 21/Sep/2020
 */
public class TrackingApp {
    public static final Logger logger = LoggerFactory.getLogger(TrackingApp.class);
    public static void main(String[] args) {
        JsoupWebCrawler webCrawler = new JsoupWebCrawler("https://www.vulog.com");
        webCrawler.start();
    }
}
