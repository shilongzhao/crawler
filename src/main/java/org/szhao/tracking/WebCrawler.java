package org.szhao.tracking;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.szhao.Utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author zsh
 * created 21/Sep/2020
 */
public abstract class WebCrawler {
//    private static final Logger logger = LoggerFactory.getLogger(WebCrawler.class);

    private volatile TrackingExecutor exec;
    // @GuardedBy("this")
    private final Set<String> urlsToCrawl = new HashSet<>();

    private final ConcurrentHashMap<String, Boolean> seen = new ConcurrentHashMap<>();

    private static final long TIMEOUT = 500;
    private static final TimeUnit UNIT = TimeUnit.MILLISECONDS;

    private final String domain;

    public WebCrawler(String startURL) {
        this.domain = startURL;
        urlsToCrawl.add(startURL);
    }

    public String getDomain() {
        return domain;
    }

    public synchronized void start() {
        exec = new TrackingExecutor(Executors.newCachedThreadPool());
        for (String url : urlsToCrawl) submitCrawlTask(url);
        urlsToCrawl.clear();
    }

    public synchronized void stop() throws InterruptedException {
        try {
            saveUncrawled(exec.shutdownNow());
            if (exec.awaitTermination(TIMEOUT, UNIT)) {
                saveUncrawled(exec.getCancelledTasks());
            }
        } finally {
            exec = null;
        }

    }

    private void saveUncrawled(List<Runnable> uncrawled) {
        for (Runnable task: uncrawled)
            urlsToCrawl.add(((CrawlTask) task).getPage());
    }

    private void submitCrawlTask(String url) {
        if (Utils.isInDomain(url, domain) && !alreadyCrawled(url)) {
            seen.put(url, true);
            exec.execute(new CrawlTask(url));
        }
    }

    boolean alreadyCrawled(String url) {
        return seen.getOrDefault(url, false);
    }

    protected abstract List<String> processPage(String url);

    public Set<String> getUrlsToCrawl() {
        return urlsToCrawl;
    }

    private class CrawlTask implements Runnable {

        private final String url;

        private CrawlTask(String url) {
            this.url = url;
        }



        void markUncrawled() {
            seen.remove(url);
        }


        @Override
        public void run() {
//            logger.info("visiting: {}", url);
            for (String link: processPage(url)) {
                if (Thread.currentThread().isInterrupted()) {
                    return;
                }
                submitCrawlTask(Utils.trimURL(link));
            }
        }

        public String getPage() {
            return url;
        }
    }

}
