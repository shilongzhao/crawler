package org.szhao;

import java.util.Set;

/**
 * @author zsh
 * created 13/Sep/2020
 */
public interface DomainLinkExtractor {
    String getDomain();
    Set<String> getChildren(String link);
}
