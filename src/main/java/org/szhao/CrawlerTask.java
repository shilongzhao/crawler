package org.szhao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

/**
 * Runnable crawler task
 *
 * @author zsh
 * created 13/Sep/2020
 */

public class CrawlerTask implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(CrawlerTask.class);

    final ExecutorService executorService;
    final ConcurrentHashMap<String, Set<String>> graph;
    final ConcurrentHashMap<String, Boolean> submitted;
    final String url;
    final DomainLinkExtractor extractor;

    public CrawlerTask(ExecutorService executorService,
                       ConcurrentHashMap<String, Set<String>> graph,
                       ConcurrentHashMap<String, Boolean> submitted,
                       String url, DomainLinkExtractor extractor) {
        this.executorService = executorService;
        this.graph = graph;
        this.submitted = submitted;
        this.url = url;
        this.extractor = extractor;
    }

    @Override
    public void run() {

        logger.info("visiting {}", url);

        Set<String> hrefs = extractor.getChildren(url);

        graph.put(url, hrefs);

        hrefs.forEach(href -> {
            if (!submitted.getOrDefault(href, false)) {
                submitted.put(href, true);
                CrawlerTask task = new CrawlerTask(executorService, graph, submitted,  href, extractor);
                executorService.submit(task);
            }
        });
    }
}
