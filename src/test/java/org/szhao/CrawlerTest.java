package org.szhao;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author zsh
 * created 13/Sep/2020
 */
public class CrawlerTest {

    @Test
    public void crawlerTest() {
        final String domain = "https://www.example.com";
        final int N_SUB_PAGES = 1_000;
        DomainLinkExtractor domainLinkExtractor = new DomainLinkExtractor() {
            @Override
            public String getDomain() {
                return domain;
            }

            // fully cross referencing subpages
            @Override
            public Set<String> getChildren(String link) {
                return IntStream.range(1, N_SUB_PAGES + 1).<String>mapToObj(i -> String.format("https://www.example.com/%d", i))
                        .collect(Collectors.toSet());
            }
        };

        Crawler crawler = new Crawler(domain, domainLinkExtractor);
        Map<String, Set<String>> sitemap = crawler.crawl();

        Assert.assertEquals(N_SUB_PAGES + 1, sitemap.size()); // root page + N_SUM_PAGES
        Assert.assertEquals(N_SUB_PAGES, sitemap.get(domain).size());
        sitemap.remove(domain);
        sitemap.values().forEach(links -> {
            Assert.assertEquals(N_SUB_PAGES, links.size());
        });
    }
}
