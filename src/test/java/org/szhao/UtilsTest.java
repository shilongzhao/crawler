package org.szhao;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.szhao.Utils.isInDomain;
import static org.szhao.Utils.trimURL;

/**
 * @author zsh
 * created 13/Sep/2020
 */
public class UtilsTest {

    @Test
    public void isInDomainTest() {
        final String domain = "https://example.com";
        assertFalse(isInDomain("https://cs.example.com", domain));
        assertFalse(isInDomain(null, domain));
        assertFalse(isInDomain("", domain));
        assertTrue(isInDomain("https://example.com/cs", domain));
        assertTrue(isInDomain(domain, domain));
    }

    @Test
    public void trimURLTest() {
        final String domain = "https://example.com";
        assertEquals(domain, trimURL(domain + "?page=1"));
        assertEquals(domain, trimURL(domain + "#fragment"));
        assertEquals(domain, trimURL(domain + "/"));
    }
}
