# Web Crawler

This is an implementation of a web crawler whose task it to generate the sitemap within the specified domain

### Environment

- Check Java
```
$ java -version
openjdk version "11.0.6" 2020-01-14
OpenJDK Runtime Environment (build 11.0.6+10-post-Debian-1bpo91)
OpenJDK 64-Bit Server VM (build 11.0.6+10-post-Debian-1bpo91, mixed mode, sharing)
```
- Check Maven

```
 mvn -v
Apache Maven 3.3.9
Maven home: /usr/share/maven
Java version: 11.0.6, vendor: Debian
Java home: /usr/lib/jvm/java-11-openjdk-amd64
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "4.9.0-12-amd64", arch: "amd64", family: "unix"
```
### Test

```
mvn clean test
```

### Run

```
mvn compile exec:java -Dexec.mainClass="org.szhao.App"
```

The `TrackingApp` will keep a record of interrupted tasks, 
so that it is possible to pick up and run the tasks next time.